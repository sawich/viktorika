
// deps
#include "sampgdk.h"

// local
#include "include/server.hpp"

bool server::OnGameModeInit (void) noexcept
{
  sampgdk::SetGameModeText ("Hello, World!");
  sampgdk::AddPlayerClass (0, 1958.3783f, 1343.1572f, 15.3746f, 269.1425f, 0, 0, 0, 0, 0, 0);
  return true;
}

bool server::OnPlayerConnect (int32_t const playerid) noexcept
{
  sampgdk::SendClientMessage (playerid, 0xFFFFFFFF, "Welcome to the HelloWorld server!");

  players.emplace (playerid, playerid);
  return true;
}

bool server::OnPlayerDisonnect (int32_t const playerid, int32_t const reason) noexcept
{
  players.erase (playerid);
  return true;
}

bool server::OnPlayerRequestClass (int32_t const playerid, int32_t const classid) noexcept
{
  sampgdk::SetPlayerPos (playerid, 1958.3783f, 1343.1572f, 15.3746f);
  sampgdk::SetPlayerCameraPos (playerid, 1958.3783f, 1343.1572f, 15.3746f);
  sampgdk::SetPlayerCameraLookAt (playerid, 1958.3783f, 1343.1572f, 15.3746f, CAMERA_CUT);
  return true;
}

bool server::OnPlayerCommandText (int32_t const playerid, const char* cmdtext) noexcept
{
  if (strcmp (cmdtext, "/hello") == 0) {
    char name[MAX_PLAYER_NAME];
    sampgdk::GetPlayerName (playerid, name, sizeof (name));
    char message[MAX_CLIENT_MESSAGE];
    sprintf (message, "Hello, %s!", name);
    sampgdk::SendClientMessage (playerid, 0x00FF00FF, message);
    return true;
  }
  return false;
}

std::unique_ptr <server> server::m_instance{ std::make_unique <server> () };
