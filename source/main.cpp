
// deps
#include "sampgdk.h"

// local
#include "include/server.hpp"

// c++
#include <stdio.h>
#include <string.h>


void SAMPGDK_CALL PrintTickCountTimer (int32_t const timerid, void* params) {
	sampgdk::logprintf ("Tick count: %d", sampgdk::GetTickCount ());
}

PLUGIN_EXPORT bool PLUGIN_CALL OnGameModeInit () {
	return server::instance ()->OnGameModeInit();
}

PLUGIN_EXPORT bool PLUGIN_CALL OnPlayerConnect (int32_t const playerid) {
	return server::instance ()->OnPlayerConnect (playerid);
}

PLUGIN_EXPORT bool PLUGIN_CALL OnPlayerDisonnect (int32_t const playerid, int32_t const reason) {
	return server::instance ()->OnPlayerDisonnect (playerid, reason);
}

PLUGIN_EXPORT bool PLUGIN_CALL OnPlayerRequestClass (int32_t playerid, int32_t classid) {
	return server::instance ()->OnPlayerRequestClass (playerid, classid);
}

PLUGIN_EXPORT bool PLUGIN_CALL OnPlayerCommandText (int32_t playerid, const char* cmdtext) {
	return server::instance ()->OnPlayerCommandText (playerid, cmdtext);
}

PLUGIN_EXPORT uint32_t PLUGIN_CALL Supports () {
	return sampgdk::Supports () | SUPPORTS_PROCESS_TICK;
}

PLUGIN_EXPORT bool PLUGIN_CALL Load (void** ppData) {
	return sampgdk::Load (ppData);
}

PLUGIN_EXPORT void PLUGIN_CALL Unload () {
	sampgdk::Unload ();
}

PLUGIN_EXPORT void PLUGIN_CALL ProcessTick () {
	sampgdk::ProcessTick ();
}
