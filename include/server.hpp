#pragma once

#include "player.hpp"

#include <unordered_map>
#include <memory>

class server
{
	static std::unique_ptr <server> m_instance;
	
public:
	bool OnGameModeInit (void) noexcept;

	bool OnPlayerConnect (int32_t const playerid) noexcept;

	bool OnPlayerDisonnect (int32_t const playerid, int32_t const reason) noexcept;

	bool OnPlayerRequestClass (int32_t const playerid, int32_t const classid) noexcept;

	bool OnPlayerCommandText (int32_t const playerid, const char* cmdtext) noexcept;

	static auto& instance (void) noexcept { return server::m_instance; }

	server (void) noexcept {}
	~server (void) noexcept {}

private:
	std::unordered_map <int32_t, player> players;
};
