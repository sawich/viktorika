#pragma once

#include "sampgdk.h"
#include <unordered_map>

class player_entry
{
public:
	bool sendClientMessage (int32_t playerid, int32_t color, const char* message) const noexcept
	{
		return sampgdk::SendClientMessage (playerid, color, message);
	}

protected:
	player_entry (int32_t const _id) noexcept : m_id{ _id } {}

	int32_t m_id;
};

class player : player_entry
{
public:
	player (int32_t const _id) noexcept : player_entry{ _id } {}
	~player (void) noexcept {}
};